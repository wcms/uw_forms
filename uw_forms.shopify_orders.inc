<?php

/**
 * @file
 * Shopify order fix.
 */

/**
 * Order sync process when webhook fails.
 *
 * When webhook fails to connect and order on shopify end has nid, sid, base_url
 * fields only you can use this logic that would try to pass all fields values
 * from webform submission to shopify order(update order).
 *
 * UW_FORMS_API_MAX_ORDER_LIMIT - there is an API call limit.
 * It uses "leaky bucket" algorithm.
 * Bucket size is 80 (shopify plus, otherwise its 40), and "leak rate" is 2 call
 * per second. This means there a limit of loading 20,000 orders in 2 seconds.
 * Limiting that to half.
 */

define('UW_FORMS_API_MAX_RESULTS', 250);
define('UW_FORMS_API_MAX_ORDER_LIMIT', 10000);

/**
 * Shopify Order overview admin page.
 *
 * Loading Shopify orders and trying to find which order was not synced with
 * webhook. Also allowing admin to re-sync it so that shopify order is updated
 * with data from webform.
 */
function uw_forms_shopify_orders($form, &$form_state) {
  module_load_include('inc', 'uw_forms', 'uw_forms.shopify');

  $date_format = 'Y-m-d';
  $yesterday = date($date_format, strtotime('-1 day'));
  $tomorrow = date($date_format, strtotime('+1 day'));
  $max_refund_date = date($date_format, strtotime('-45 day'));

  $table_data = array();

  // Default state for "Sync orders" button.
  $submit_state = array(
    '#disabled' => TRUE,
  );

  // Case when Refresh button is clicked and form is rebuilding, states
  // attribute needs to be updated so that Sync button is enabled if there
  // is any order selected. Otherwise it should be disabled to prevent action.
  if (!empty($form_state['values']['date_from']) && !empty($form_state['values']['date_to'])) {
    $table_data = _uw_forms_shopify_orders_prepare($form_state['values']);
    $empty_message = t('No orders found or all look valid.');

    if (!empty($table_data)) {
      $submit_state = array(
        '#states' => array(
          'enabled' => array(
            array(':input[name^="table_orders"]' => array('checked' => TRUE)),
            array(':input[title^="Select all"]' => array('checked' => TRUE)),
          ),
        ),
      );
    }
  }
  else {
    $empty_message = t('No data available. Refresh table.');
  }

  // Creating a form to display "broken" orders in selected date range.
  // There are two date fields (dependency on Date module), refresh button,
  // table and sync button.
  $form['date_from'] = array(
    '#type' => 'date_popup',
    '#date_format' => $date_format,
    '#date_year_range' => '0:0',
    '#title' => t('From date'),
    '#date_label_position' => 'within',
    '#default_value' => $yesterday,
    '#datepicker_options' => array('minDate' => $max_refund_date, 'maxDate' => $yesterday),
  );

  $form['date_to'] = array(
    '#type' => 'date_popup',
    '#date_format' => $date_format,
    '#date_year_range' => '0:0',
    '#title' => t('To date'),
    '#date_label_position' => 'within',
    '#default_value' => $tomorrow,
    '#datepicker_options' => array('minDate' => $max_refund_date, 'maxDate' => $tomorrow),
  );

  // Ajax call is being used to refresh content of the table. It will replace
  // DOM element id "shopify_table" with new content once ajax call completes.
  $form['table_refresh'] = array(
    '#type' => 'button',
    '#value' => t('Refresh table'),
    '#ajax' => array(
      'callback' => '_ajax_uw_forms_shopify_table_refresh',
      'wrapper' => 'shopify_table',
    ),
    '#submit' => array('uw_forms_shopify_table_refresh'),
    '#suffix' => '<br><br><hr>',
  );

  $header = array(
    'order_id' => t('Order'),
    'order_date' => t('Order date'),
    'email' => t('Email'),
    'first_name' => t('First name'),
    'last_name' => t('Last name'),
    'webform_submission' => t('Webform submission'),
  );

  // Container is used to refresh table and sync button. This is done because of
  // states of the button.
  $form['container'] = array(
    '#type' => 'container',
  );

  $form['container']['table_orders'] = array(
    '#type' => 'tableselect',
    '#header' => $header,
    '#options' => $table_data,
    '#empty' => $empty_message,
    '#prefix' => '<div id="shopify_table">',
  );

  // Default state is applied last, that state depends on content of the table.
  $form['container']['fixorders'] = array(
    '#type' => 'submit',
    '#value' => t('Sync orders'),
    '#suffix' => '</div>',
  ) + $submit_state;

  return $form;
}

/**
 * Using FORM API state to rebuild form and include orders from Shopify.
 *
 * Make sure to rebuild form once Refresh button is clicked. This will force
 * sync button to change state depending on table contents.
 *
 * @param array $form
 *   Admin form.
 * @param array $form_state
 *   Admin form state with values.
 */
function uw_forms_shopify_table_refresh(array $form, array &$form_state) {
  $form_state['rebuild'] = TRUE;
}

/**
 * Form state replace element.
 *
 * Returns part of the form that needs to be updated based on ajax call.
 * Two fields require update: table and sync button.
 *
 * @param array $form
 *   Admin form.
 * @param array $form_state
 *   Admin form state with values.
 *
 * @return mixed
 *   Returns elements that will be replaced with new one.
 */
function _ajax_uw_forms_shopify_table_refresh(array $form, array &$form_state) {
  return $form['container'];
}

/**
 * Form validate function.
 *
 * Validate dates and if there are any selected orders. Even though submit
 * button is disabled when there are no orders selected this is
 * checked to be sure.
 *
 * @param array $form
 *   Admin form.
 * @param array $form_state
 *   Admin form state with values.
 */
function uw_forms_shopify_orders_validate(array $form, array &$form_state) {
  $values = $form_state['values'];

  // Check if date range makes sense (start < end).
  $date_from = strtotime($values['date_from']);
  $date_to = strtotime($values['date_to']);

  if (!$date_from || !$date_to) {
    $error_message = t('Date empty or not valid, please enter valid date.');

    if (!$date_from) {
      form_set_error('date_from', $error_message);
    }
    else {
      form_set_error('date_to', $error_message);
    }
  }
  elseif ($date_to < $date_from) {
    form_set_error('date_from',
      t('Date range not valid, make sure to date is greater than from date.')
    );
  }
  else {
    // If Refresh table button is clicked skip table data validation.
    // Since initially table is empty. And that could break validation.
    if ($form_state['values']['op'] != 'Refresh table') {
      // Check if there is any row selected.
      $table = array_filter($values['table_orders']);

      if (empty($table)) {
        form_set_error('table', t('No records selected.'));
      }
    }
  }
}

/**
 * Order submit handler.
 *
 * Submit handler that will load selected orders again (form API) and
 * invoke same function webhook does. This action will update orders on
 * shopify end with all webform fields.
 *
 * @param array $form
 *   Admin form.
 * @param array $form_state
 *   Admin form state with values.
 */
function uw_forms_shopify_orders_submit(array $form, array &$form_state) {
  module_load_include('inc', 'uw_forms', 'uw_forms.shopify');

  // Prepare selected orders to be loaded again using API.
  $values = $form_state['values'];
  $table = array_filter($values['table_orders']);
  $orders = implode(',', array_values($table));
  $path = 'admin/orders.json?ids=' . $orders;

  // Wrapper was not used here in order to maintain data format for function
  // Uw_forms_shopify_api_webhook_process (it expects order to be object
  // not array).
  $api_result = uw_shopify_api_call($path);

  if ($api_result['status'] == 200 && $api_result['errno'] == 0) {
    // Using json_decode instead of drupal wrapper to be consistent
    // with existing logic.
    $rows = json_decode($api_result['result']);

    // Iterate rows and invoke uw_forms_shopify_api_webhook_process (row).
    foreach ($rows->orders as $order) {
      try {
        uw_forms_shopify_api_webhook_process($order);
        drupal_set_message(t('Order: @order synced successfully.',
          ['@order' => $order->order_number]
        ));
      }
      catch (Exception$ex) {
        watchdog('uw_forms.shopify_orders', $ex->getMessage(), WATCHDOG_ERROR);
        drupal_set_message(t('Error with order: @order. Check log for more details.',
          ['@order' => $order->order_number]
        ));
      }
    }
  }
}

/**
 * Preparing orders to be displayed in a table.
 *
 * When loading orders from shopify make sure those orders have note_attributes:
 * - nid
 * - sid
 * - base_url
 * - token.
 *
 * Only then display order in this table. This is done to filter out only those
 * orders that webhook failed to sync with drupal. Other orders will be skipped.
 *
 * @param array $values
 *   Form values passed as argument.
 *
 * @return array
 *   Returning order table rows.
 */
function _uw_forms_shopify_orders_prepare(array $values) {
  // Convert dates to iso 8601 per API requirement, use date('c').
  $date_from = date('c', strtotime($values['date_from']));
  $date_to = date('c', strtotime($values['date_to']));
  $table_data = array();

  $path = 'admin/orders/count.json?status=any&created_at_min=' . $date_from . '&created_at_max=' . $date_to;
  $api_count = _uw_forms_shopify_order_api_wrapper($path);
  $count = isset($api_count['count']) ? (int) $api_count['count'] : 0;

  if ($count <= UW_FORMS_API_MAX_ORDER_LIMIT) {
    $requests = ceil((int) $count / UW_FORMS_API_MAX_RESULTS);

    for ($api_page = 1; $api_page <= $requests; $api_page++) {
      // Prepare URL that would load all order in the specific date range and
      // use result limit also page (API argument) when there are more than 250
      // orders available for date range.
      $path = 'admin/orders.json?status=any&created_at_min=' . $date_from . '&created_at_max=' . $date_to . '&limit=' . UW_FORMS_API_MAX_RESULTS . '&page=' . $api_page;

      $rows = _uw_forms_shopify_order_api_wrapper($path);

      if (isset($rows['orders']) && !empty($rows['orders'])) {
        foreach ($rows['orders'] as $order) {
          $validated_order = _uw_forms_shopify_order_validate_order($order);

          if (!empty($validated_order)) {
            $table_data[$order['id']] = $validated_order;
          }
        }
      }
    }
  }
  else {
    drupal_set_message(
      t('There are @num orders in specified date range which is over allowed limit of @max. Please adjust date range.',
        array('@num' => $count, '@max' => UW_FORMS_API_MAX_ORDER_LIMIT)
      ),
      'error'
    );
  }

  return $table_data;
}

/**
 * Check if this order needs to be considered as not valid.
 *
 * Check individual order for sid, nid, base_url and token. If order has all
 *   those fields then webhook did not connect or there was an error while
 *   doing order update.
 *
 * Create a list of order that are not synced.
 * Returns empty array if order is valid or array of data for table
 *
 * @param array $order
 *   Order with all its data as array.
 *
 * @return array
 *   Order array that will be displayed as row in a table.
 */
function _uw_forms_shopify_order_validate_order(array $order) {
  $validated_order = array();

  if (isset($order['note_attributes'])
    && !empty($order['note_attributes'])
    && is_array($order['note_attributes'])
  ) {

    global $base_url;
    $shopify_domain = variable_get('uw_shopify_domain');

    // Using assoc array for easier remove by key.
    $required_attributes = array(
      'nid' => 0,
      'sid' => 0,
      'base_url' => 0,
      'token' => 0,
    );

    foreach ($order['note_attributes'] as $item) {
      if (isset($required_attributes[$item['name']])) {
        $required_attributes[$item['name']] = $item['value'];
      }
    }

    $validate_required = array_filter($required_attributes);

    // If this is met it means all required attributes are found in order
    // 4 = count(required_required).
    if (count($validate_required) === 4 && $validate_required['base_url'] === $base_url) {
      $validated_order['order_date'] = $order['created_at'];
      $validated_order['first_name'] = $order['customer']['first_name'];
      $validated_order['last_name'] = $order['customer']['last_name'];
      $validated_order['email'] = $order['email'];

      // Create webform submission link based on info from shopify order.
      $validated_order['webform_submission'] = l(
        t('link'),
        'node/' . $required_attributes['nid'] .
        '/submission/' . $required_attributes['sid'],
        array('attributes' => array('target' => '_blank'))
      );

      // Create Shopify order link. This requires access to Shopify dashboard
      // in order to view this link. If not logged in to Shopify you will be
      // redirected to login page. Not all will have access to this link.
      $order_link = l(
        $order['order_number'],
        '//' . $shopify_domain . '/admin/orders/' . $order['id'],
        array('attributes' => array('target' => '_blank'))
      );

      $validated_order['order_id'] = $order_link;
    }
  }

  return $validated_order;
}

/**
 * Shopify API wrapper.
 *
 * @param string $path
 *   Wrapper to existing API call function found in uw_forms.shopify.inc.
 *
 * @return array|mixed
 *   Empty array or order array is the result.
 */
function _uw_forms_shopify_order_api_wrapper($path) {
  $rows = array();
  $api_result = uw_shopify_api_call($path);

  if ($api_result['status'] === 200 && $api_result['errno'] === 0) {
    $rows = drupal_json_decode($api_result['result']);
  }

  return $rows;
}
