<?php

/**
 * @file
 * Functions related to Shopify integration.
 */

/**
 * Takes token data and returns the URL to redirect the user to.
 *
 * @param array $data
 *   Array with keys node containing the node object and webform-submission
 *   containing the webform submission object.
 *
 * @return string
 *   The URL of the payment page to redirect the user to.
 */
function uw_forms_shopify_create_redirect(array $data, $name) {
  // Create easy-to-access array with all form values.
  $webform_submission = uw_forms_webform_submission_add_by_form_key($data['webform-submission']);
  $values =& $webform_submission->data_by_form_key;

  // Create Shopify array with transcript order and order data.
  _uw_forms_shopify_load_form_include($name);
  $get_varient_func = 'uw_forms_get_varients_' . str_replace('-', '_', $name);
  $shopify = [
    'order' => $get_varient_func($values),
    'data' => [
      'checkout' => [
        'email' => $values['email'][0],
        'billing_address' => [
          'first_name' => $values['name_first'][0],
          'last_name' => $values['name_last'][0],
        ],
      ],
    ],
  ];
  if (isset($values['telephone_number'][0])) {
    $shopify['data']['checkout']['billing_address']['phone'] = $values['telephone_number'][0];
  }

  if (isset($values['address'])) {
    $address_components = [
      'addr_1' => 'address1',
      'addr_2' => 'address2',
      'city' => 'city',
      'country' => 'country_code',
      'province' => 'province_code',
      'postal_code' => 'zip',
    ];
    foreach ($address_components as $address_component => $shopify_name) {
      if (isset($values['address'][$address_component])) {
        $shopify['data']['checkout']['billing_address'][$shopify_name] = $values['address'][$address_component];
      }
    }
  }

  global $base_url;
  $shopify['data']['attributes']['base_url'] = $base_url;
  $shopify['data']['attributes']['nid'] = $data['node']->nid;
  $shopify['data']['attributes']['sid'] = $data['webform-submission']->sid;
  $shopify['data']['attributes']['token'] = webform_get_submission_access_token($data['webform-submission']);

  return uw_shopify_get_multipass_url($shopify);
}

/**
 * Loads a form-specific code file.
 *
 * @param string $name
 *   The machine name of the form to load.
 *
 * @return string|false
 *   The name of the included file, if successful; FALSE otherwise.
 */
function _uw_forms_shopify_load_form_include($name) {
  $filename = conf_path() . '/forms/' . $name . '.php';
  if (is_file($filename)) {
    require_once $filename;
    return $filename;
  }
  else {
    return FALSE;
  }
}

/**
 * Return the base URL for the Shopify store.
 *
 * @param bool $authenticated
 *   Whether to include the API key and password in the URL.
 *
 * @return string
 *   The URL.
 */
function uw_shopify_get_store($authenticated = FALSE) {
  if (!variable_get('uw_shopify_domain')) {
    throw new Exception('uw_shopify_domain not configured.');
  }
  if ($authenticated) {
    if (!variable_get('uw_shopify_api_key') || !variable_get('uw_shopify_api_password')) {
      throw new Exception('uw_shopify_api_key|password not configured.');
    }
    $authenticated = variable_get('uw_shopify_api_key') . ':' . variable_get('uw_shopify_api_password') . '@';
  }
  else {
    $authenticated = '';
  }
  return 'https://' . $authenticated . variable_get('uw_shopify_domain') . '/';
}

/**
 * Return a Shopify Multipass URL.
 *
 * @param array $shopify
 *   An array of information representing a Shopify order.
 *
 * @return string
 *   The URL.
 *
 * @see https://help.shopify.com/api/reference/multipass
 */
function uw_shopify_get_multipass_url(array $shopify) {
  if (!variable_get('uw_shopify_multipass_secret')) {
    throw new Exception('uw_shopify_multipass_secret not configured.');
  }
  $customer_data = [
    'email' => $shopify['data']['checkout']['email'],

    'first_name' => $shopify['data']['checkout']['billing_address']['first_name'],
    'last_name' => $shopify['data']['checkout']['billing_address']['last_name'],

    // This is desirable for security, but incompatible with the UW VPN.
    // 'remote_ip' => ip_address(),
    'return_to' => uw_shopify_create_cart_permalink($shopify),
  ];

  foreach (array_keys($shopify['data']['checkout']['billing_address']) as $key) {
    $customer_data['addresses'][0][$key] = $shopify['data']['checkout']['billing_address'][$key];
  }

  $multipass = new ShopifyMultipass(variable_get('uw_shopify_multipass_secret'));
  return uw_shopify_get_store() . 'account/login/multipass/' . $multipass->generate_token($customer_data);
}

/**
 * Make a HTTP call to the Shopify API.
 *
 * @param string $path
 *   The URL path to make the request to.
 * @param string $body
 *   The request body. If specified, the request will use PUT, otherwise GET.
 *
 * @return array
 *   Array with the following keys, each of which will be FALSE if curl_init()
 *   fails:
 *    - status: The integer HTTP status code.
 *    - result: The result of the query.
 *    - errno: An error number, zero for no error.
 *    - error: An error message if there is one, otherwise the empty string.
 *
 * @see https://help.shopify.com/api/reference/order
 */
function uw_shopify_api_call($path, $body = NULL) {
  $return = [
    'status' => FALSE,
    'result' => FALSE,
    'errno' => FALSE,
    'error' => FALSE,
  ];
  $url = uw_shopify_get_store(TRUE) . $path;
  if ($ch = curl_init($url)) {
    $headers = [];
    $headers[] = 'Accept: application/json';

    // Use proxy server.
    curl_setopt($ch, CURLOPT_PROXY, 'https://calamar.uwaterloo.ca:3128');

    if ($body) {
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');

      $body = json_encode($body);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $body);

      $headers[] = 'Content-Length: ' . strlen($body);
      $headers[] = 'Content-Type: application/json';
    }

    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    $return['result'] = curl_exec($ch);
    $return['status'] = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    $return['errno'] = curl_errno($ch);
    $return['error'] = curl_error($ch);
    if ($return['error']) {
      watchdog('uw_forms', 'uw_shopify_api_call error @errno: @error', ['@errno' => $return['errno'], '@error' => $return['error']], WATCHDOG_CRITICAL);
    }
    curl_close($ch);
  }
  return $return;
}

/**
 * Generate a Shopify permalink.
 *
 * $param array $shopify
 *   Array with keys:
 *    - 'order': Array of varient ID => quantity.
 *    - 'data': Array to be turned into the query string in the URL.
 *
 * @return string
 *   The URL.
 *
 * @see https://help.shopify.com/themes/customization/cart/use-permalinks-to-preload-cart
 */
function uw_shopify_create_cart_permalink(array $shopify) {
  $path = uw_shopify_get_store() . 'cart/';

  $path .= uw_shopify_create_order_string($shopify['order']);

  $options = ['query' => $shopify['data']];

  return url($path, $options);
}

/**
 * Generate a Shopify order string.
 *
 * This is a comma-separated list of "<variant-id>:<quantity>".
 *
 * $param array $order
 *   Array of varient ID => quantity.
 *
 * @return string
 *   The order string.
 */
function uw_shopify_create_order_string(array $order) {
  ksort($order);
  $varients = [];
  foreach ($order as $varient => $quantity) {
    $varients[] = (int) $varient . ':' . (int) $quantity;
  }
  return implode(',', $varients);
}

/**
 * Page callback for Shopify webhook.
 *
 * Shopify will POST to this a JSON object when an order is created. This
 * function attemps to process it, sets a log message if it fails, and returns a
 * 200 status on an empty page to notify Shopify that the webhook was recieved.
 */
function uw_forms_shopify_api_webhook() {
  if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $input = json_decode(file_get_contents('php://input'));

    try {
      uw_forms_shopify_api_webhook_process($input);
    }
    catch (Exception $e) {
      watchdog('uw_forms', 'Shopify webhook failed: @reason', ['@reason' => $e->getMessage()], WATCHDOG_CRITICAL);
    }
  }

  // Deliver a blank page. Shopify needs to get a 200 status but otherwise
  // ignores what is returned.
  drupal_deliver_page(NULL);
}

/**
 * Validate the webhook and set attributes if it passes.
 *
 * If the webhook validates use the Shopify API to set various attributes on the
 * order, such as the shipping address.
 */
function uw_forms_shopify_api_webhook_process($input) {
  variable_set('shopify-last', $input);

  if (!isset($input->note_attributes)) {
    throw new Exception('note_attributes missing from webhook. Order: ' . $input->name);
  }
  $attributes = [];
  foreach ($input->note_attributes as $attribute) {
    $attributes[$attribute->name] = $attribute->value;
  }

  // Return if request is for a different site.
  global $base_url;
  if (empty($attributes['base_url']) || $attributes['base_url'] !== $base_url) {
    return;
  }

  variable_set('shopify-last-attributes', $attributes);

  // nid, sid, and submission_access_token must be set.
  foreach (['nid', 'sid', 'token'] as $field) {
    if (!isset($attributes[$field])) {
      throw new Exception('Variable missing from webhook: ' . $field . '. Order: ' . $input->name);
    }
  }

  // Load submission.
  module_load_include('inc', 'webform', 'includes/webform.submissions');
  $submission = webform_get_submission($attributes['nid'], $attributes['sid']);
  $submission = uw_forms_webform_submission_add_by_form_key($submission);

  variable_set('shopify-last-sub', $submission);

  // Verify that the submission_access_token in the POST is valid.
  if ($attributes['token'] !== webform_get_submission_access_token($submission)) {
    throw new Exception('submission_access_token does not match. Order: ' . $input->name);
  }

  // Verify that all email addresses match.
  $original_email = $submission->data_by_form_key['email'][0];
  if ($input->email !== $original_email || $input->contact_email !== $original_email || $input->customer->email !== $original_email) {
    throw new Exception('Email address does not match. Order: ' . $input->name);
  }

  $node = node_load($attributes['nid']);
  if (preg_match('/^\[uw-forms-ecomm-urls:(.+)\]$/', $node->webform['redirect_url'], $backref)) {
    $ecomm_name = $backref[1];
    _uw_forms_shopify_load_form_include($ecomm_name);
  }
  else {
    throw new Exception('Form (' . intval($attributes['nid']) . ') not configured for Shopify. Order: ' . $input->name);
  }

  // Verify that items purchased have not changed. Without this, users could,
  // for example, remove shipping costs from their cart.
  $order_from_shopify = [];
  foreach ($input->line_items as $item) {
    $order_from_shopify[$item->variant_id] = $item->quantity;
  }
  $order_from_shopify = uw_shopify_create_order_string($order_from_shopify);
  $get_varient_func = 'uw_forms_get_varients_' . str_replace('-', '_', $ecomm_name);
  if (!function_exists($get_varient_func)) {
    throw new Exception('Function does not exist: ' . $get_varient_func . '. Order: ' . $input->name);
  }
  $order_from_webform = uw_shopify_create_order_string($get_varient_func($submission->data_by_form_key));
  if ($order_from_shopify !== $order_from_webform) {
    throw new Exception('Cart has been edited. Order: ' . $input->name);
  }

  // Mark submission paid.
  $submission->data_by_form_key['paid'][0] = $input->name;
  webform_submission_update($node, $submission);

  // Add the shipping address and other attributes to the order on Shopify.
  $update = [
    'order' => [
      'id' => $input->id,
    ],
  ];
  // Additional note_attributes.
  $update['order']['note_attributes']['nid'] = $attributes['nid'];
  $update['order']['note_attributes']['sid'] = $attributes['sid'];
  foreach ($submission->data_by_form_key as $key => $value) {
    $excluded_fields = [
      // Included with account creation.
      'name_first',
      'name_last',
      'email',
      // Added separately below.
      'address',
      // Shopify has its own fields for tracking this.
      'paid',
    ];
    if (in_array($key, $excluded_fields, TRUE)) {
      continue;
    }
    $value = implode(', ', array_filter($value));
    if ($value) {
      $update['order']['note_attributes'][$key] = $value;
    }
  }
  // shipping_address.
  if (isset($submission->data_by_form_key['address'])) {
    $update['order']['shipping_address']['first_name'] = $submission->data_by_form_key['name_first'][0];
    $update['order']['shipping_address']['last_name'] = $submission->data_by_form_key['name_last'][0];
    $update['order']['shipping_address']['phone'] = $submission->data_by_form_key['destination_phone'][0] ?: $submission->data_by_form_key['telephone_number'][0];
    $address_components = [
      'addr_1' => 'address1',
      'addr_2' => 'address2',
      'city' => 'city',
      'country' => 'country_code',
      'province' => 'province',
      'postal_code' => 'zip',
    ];
    foreach ($address_components as $address_component => $shopify_name) {
      $update['order']['shipping_address'][$shopify_name] = isset($submission->data_by_form_key['address'][$address_component]) ? $submission->data_by_form_key['address'][$address_component] : NULL;
    }
    // Shipping address must be complete or not there at all.
    if (!$update['order']['shipping_address']['address1'] || !$update['order']['shipping_address']['country_code'] || !$update['order']['shipping_address']['zip']) {
      unset($update['order']['shipping_address']);
    }
  }

  $result = uw_shopify_api_call('admin/orders/' . $input->id . '.json', $update);
  $result['result'] = json_decode($result['result']);

  variable_set('shopify-last-update', $update);
  variable_set('shopify-curl-result', $result);

  if ((int) $result['status'] !== 200) {
    $params = [
      '@status' => $result['status'],
      '@error' => json_encode($result['result']),
      '@order' => $input->name,
    ];
    throw new Exception(t('Webhook returned status @status: @error. Order: @order', $params));
  }
}

/**
 * Display values from the last Shopify webhook call for debugging.
 *
 * @return string
 *   A blank page or a message enabling the devel module.
 */
function uw_forms_shopify_api_webhook_view() {
  if (function_exists('dsm')) {
    dsm(variable_get('shopify-last'), 'shopify-last');
    dsm(variable_get('shopify-last-attributes'), 'shopify-last-attributes');
    dsm(variable_get('shopify-last-sub'), 'shopify-last-sub');
    dsm(variable_get('shopify-last-update'), 'shopify-last-update');

    dsm(variable_get('shopify-curl-result'), 'shopify-curl-result');
    return '';
  }
  else {
    return 'Enable devel module and reload to see messages.';
  }
}

/**
 * Display results from testing connection to the Shopify API.
 *
 * @return string
 *   A blank page or a message enabling the devel module.
 */
function uw_forms_shopify_api_webhook_test() {
  if (function_exists('dsm')) {
    $result = uw_shopify_api_call('admin/webhooks.json');
    $result['result'] = json_decode($result['result']);
    dsm($result);
    return '';
  }
  else {
    return 'Enable devel module and reload to see messages.';
  }
}

/**
 * Generate multipass login link with values from webform.
 *
 * @param array $data
 *   Webform submission field values.
 *
 * @return string
 *   Multipass link with customer data
 *
 * @throws \Exception
 *    When missing shop configuration.
 */
function uw_forms_shopify_login(array $data) {
  $webform_submission = uw_forms_webform_submission_add_by_form_key($data['webform-submission']);
  $values =& $webform_submission->data_by_form_key;

  if (!variable_get('uw_shopify_multipass_secret')) {
    throw new Exception('uw_shopify_multipass_secret not configured.');
  }

  // You can specify shopify landing page by setting variable in settings.php.
  $store_landing_page = variable_get('uw_shopify_multipass_landing_page', '/');

  $customer_data = [
    'email' => $values['email'][0],
    'first_name' => $values['name_first'][0],
    'last_name' => $values['name_last'][0],
    'return_to' => $store_landing_page,
    'identifier' => $values['cas_id'][0],
  ];

  $multipass = new ShopifyMultipass(variable_get('uw_shopify_multipass_secret'));
  return uw_shopify_get_store() . 'account/login/multipass/' . $multipass->generate_token($customer_data);
}
