/**
 * @file
 */

(function ($) {
  $(document).ready(function () {
    $('div.webform-component-uwaddress input.webform-component-uwaddress-postal_code, div.webform-component-uwaddress select.webform-component-uwaddress-country').change(function () {
      var component, addr_1, city, province, country, postal_code, code;
      component = $(this).closest('div.webform-component-uwaddress');

      postal_code = component.find('input.webform-component-uwaddress-postal_code');
      country = component.find('select.webform-component-uwaddress-country');
      addr_1 = component.find('input.webform-component-uwaddress-addr_1');
      city = component.find('input.webform-component-uwaddress-city');
      province = component.find('input.webform-component-uwaddress-province');

      postal_code.val(postal_code.val().trim().toUpperCase());

      // Format CA postal codes.
      if (country.val() === 'CA' && postal_code.val().length === 6 && /^[A-Z]\d[A-Z] ?\d[A-Z]\d$/.test(postal_code.val())) {
        postal_code.val(postal_code.val().substr(0, 3) + ' ' + postal_code.val().substr(3));
      }

      // If a postal code starts with two letters and a hyphen, take that to be the country code. Set the country and remove the country code from the postal code.
      code = postal_code.val().match(/^([A-Z]{2})-([\w \-]+)/);
      if (code && country.find('option[value="' + code[1] + '"]')) {
        country.val(code[1]);
        postal_code.val(code[2]);
      }

      // Make Ajax request to geocode the postal code and set the values when the results come back.
      if (country.val() && postal_code.val()) {
        $.ajax(Drupal.settings.basePath + 'uw_forms/geocoder/' + country.val() + '/' + postal_code.val()).done(function (data) {
          // Postal Code.
          if (data.postal_code) {
            postal_code.val(data.postal_code);
          }

          // Country.
          if (data.country) {
            country.val(data.country);
          }

          // Address.
          if (data.address && !addr_1.val()) {
            addr_1.val(data.address);
          }

          // City.
          if (data.city) {
            city.val(data.city);
          }

          // Province.
          if (data.province) {
            province.val(data.province);
          }
        });
      }

    });
  });
}(jQuery));
