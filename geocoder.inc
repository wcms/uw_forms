<?php

/**
 * @file
 * Page callback and support functions for uw_forms/geocoder/%/%.
 */

/**
 * Return JSON geocoding data for a postal code.
 */
function uw_forms_geocoder($country, $postal_code) {
  $postal_code = drupal_strtoupper(trim($postal_code));
  $country = drupal_strtoupper(trim($country));

  // If the country is the default, CA, then query for any match, otherwise,
  // trust the country selection.
  if ($country === 'CA' || !preg_match('/^[A-Z]{2}$/', $country)) {
    $query = postal_code_validation_validate($postal_code);
  }
  else {
    $query = postal_code_validation_validate($postal_code, $country);
  }

  // If no error, geocode CA/US with Geocoder.ca.
  if (!$query['error']) {
    $country = $query['country'];
    $dependencies = postal_code_validation_get_dependencies();
    if (isset($dependencies[$country])) {
      $country = $dependencies[$country];
    }
    if (in_array($country, array('CA', 'US'), TRUE)) {
      $geocode = uw_forms_geocoder_ca_query($query['postal_code']);
      if ($geocode) {
        $query += $geocode;
      }
    }
  }

  header('Content-Type: application/json');
  echo json_encode($query);
  drupal_exit();
}

/**
 * Query geocoder.ca.
 */
function uw_forms_geocoder_ca_query($postal_code) {
  if ($postal_code === 'H0H 0H0') {
    return array(
      'country' => 'CA',
      'province' => 'NU',
      'city' => 'North Pole',
      'address' =>
      'Santa\'s Workshop',
    );
  }

  // ZIP+4 codes are fine, but the additional delivery point digits must be
  // removed if present.
  $postal_code_lookup = drupal_substr($postal_code, 0, 10);

  $json = file_get_contents('https://geocoder.ca/?' . http_build_query(array(
    'geoit' => 'xml',
    'jsonp' => 1,
    'callback' => 'x',
    'standard' => 1,
    'postal' => $postal_code_lookup,
  )));
  // Convert from JSON-P to JSON: Strip first and last two characters, which are
  // "x(" and ");".
  $json = drupal_substr($json, 2);
  $json = drupal_substr($json, 0, -2);

  $json = json_decode($json, TRUE);

  if ($json) {
    foreach (array('stnumber', 'staddress', 'city', 'prov') as $key) {
      $output[$key] = !empty($json['standard'][$key]) ? trim($json['standard'][$key]) : '';
    }

    // Rename prov to province.
    $output['province'] = $output['prov'];
    unset($output['prov']);

    // US dependencies use US ZIP code, but have their own country code.
    $dependencies = postal_code_validation_get_dependencies();
    if (isset($dependencies[$output['province']]) && $dependencies[$output['province']] === 'US') {
      $output['country'] = $output['province'];
    }
    elseif (preg_match('/^\d/', $postal_code)) {
      $output['country'] = 'US';
    }
    else {
      $output['country'] = 'CA';
    }

    // If case is mixed, assume it is correct. Otherwise, ensure first letters
    // of words are upper, rest lower.
    foreach (array('staddress', 'city') as $key) {
      if ($output[$key] === drupal_strtoupper($output[$key]) || $output[$key] === drupal_strtolower($output[$key])) {
        $output[$key] = ucwords(drupal_strtolower($output[$key]));
      }
    }

    // Create address if both components are present.
    if (!empty($output['stnumber']) && !empty($output['staddress'])) {
      $output['address'] = $output['stnumber'] . ' ' . $output['staddress'];
    }
    else {
      $output['address'] = '';
    }
    unset($output['stnumber']);
    unset($output['staddress']);

    return $output;
  }
  else {
    return FALSE;
  }
}
