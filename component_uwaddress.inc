<?php

/**
 * @file
 * Implements the uwaddress webform component.
 */

/**
 * Implements _webform_defaults_component().
 */
function _webform_defaults_uwaddress() {
  return array(
    'name' => '',
    'form_key' => NULL,
    'required' => 0,
    'pid' => 0,
    'weight' => 0,
    'extra' => array(
      'description' => '',
      'private' => FALSE,
    ),
  );
}

/**
 * Implements _webform_edit_component().
 */
function _webform_edit_uwaddress($component) {
  $form = array();
  // The return array has to be non-empty() or a warning message will appear.
  $form['extra'] = array();
  return $form;
}

/**
 * Implements _webform_render_component().
 */
function _webform_render_uwaddress($component, $value = NULL, $filter = TRUE) {
  drupal_add_js(drupal_get_path('module', 'uw_forms') . '/uw_forms.js');

  $wrapper_classes = array(
    'form-item',
    'webform-component',
    'webform-component-uwaddress',
    'webform-component--' . $component['form_key'],
  );
  $form_item = array(
    '#type' => 'fieldset',
    '#title' => $filter ? webform_filter_xss($component['name']) : $component['name'],
    '#required' => $component['required'],
    '#weight' => $component['weight'],
    '#description'   => $filter ? webform_filter_descriptions($component['extra']['description']) : $component['extra']['description'],
    '#theme_wrappers' => array('fieldset', 'webform_element'),
    '#element_validate' => array('uw_forms_component_uwaddress_validate'),
  );

  $weight = 0;
  foreach (uw_forms_component_uwaddress_subcomponents() as $field_key => $title) {
    $form_item[$field_key] = array(
      '#type' => 'textfield',
      // Manually add the form_required_marker here. If done by setting
      // #required, saving a draft will not work.
      '#title' => $title . (($component['required'] && !in_array($field_key, array('addr_2', 'province'), TRUE)) ? ' ' . theme('form_required_marker') : ''),
      '#weight' => $weight++,
      '#attributes' => array('class' => array('webform-component-uwaddress-' . $field_key)),
      '#default_value' => isset($value[$field_key]) ? $value[$field_key] : NULL,
    );
  }

  // Address line 2 no visible label.
  $form_item['addr_2']['#title_display'] = 'invisible';

  // Country is a select list.
  $form_item['country']['#type'] = 'select';
  include_once DRUPAL_ROOT . '/includes/locale.inc';
  $form_item['country']['#options'] = country_get_list();
  $form_item['country']['#default_value'] = $form_item['country']['#default_value'] ? $form_item['country']['#default_value'] : 'CA';

  // Display at the top so that they are filled-in first allowing
  // autocompletion.
  $form_item['postal_code']['#weight'] = -2;
  $form_item['country']['#weight'] = -1;

  return $form_item;
}

/**
 * Validate the values of a uwaddress component.
 */
function uw_forms_component_uwaddress_validate($element, &$form_state, $form) {
  // Raise an error for empty subcomponents if component is required or if part
  // of an address has been entered.
  $empty = TRUE;
  foreach (array_keys(uw_forms_component_uwaddress_subcomponents()) as $component) {
    if (!empty($element[$component]['#value']) && $component !== 'country') {
      $empty = FALSE;
    }
  }
  if ($element['#required'] || !$empty) {
    foreach (uw_forms_component_uwaddress_subcomponents() as $sub_key => $sub_title) {
      if (empty($element[$sub_key]['#value'])) {
        $this_error = FALSE;
        switch ($sub_key) {
          // Always may be empty.
          case 'addr_2':
          case 'province':
            break;

          // Always must have a value.
          case 'addr_1':
          case 'city':
          case 'country':
            $this_error = TRUE;
            break;

          // Must have value if country supported by postal_code_validation.
          case 'postal_code':
            if (in_array($element['country']['#value'], array_merge(postal_code_validation_get_supported_countries(), array_keys(postal_code_validation_get_dependencies())))) {
              $this_error = TRUE;
            }
            break;
        }
        if ($this_error) {
          if ($element['#required']) {
            // Text guaranteed sanitized.
            form_error($element[$sub_key], t('!name, !subname field is required.', array('!name' => $element['#title'], '!subname' => $sub_title)));
          }
          else {
            // Text guaranteed sanitized.
            form_error($element[$sub_key], t('!name, !subname field is required if any part of the address is filled-in.', array('!name' => $element['#title'], '!subname' => $sub_title)));
          }
        }
      }
    }
  }

  if (strval($element['postal_code']['#value']) !== '') {
    $country = $element['country']['#value'];
    $postal_code = postal_code_validation_validate($element['postal_code']['#value'], $country);

    // Postal code has error.
    if ($postal_code['error']) {
      form_set_error(implode('][', $element['postal_code']['#array_parents']), $postal_code['error']);
    }
    // Valid, set formatted version if different.
    elseif ($postal_code['postal_code'] !== $element['postal_code']['#value']) {
      form_set_value($element['postal_code'], $postal_code['postal_code'], $form_state);
    }

    // If CA or US, set province to match postal code.
    if (in_array($country, array('CA', 'US'), TRUE) && $postal_code && $postal_code['province'] && $postal_code['province'] !== $element['province']['#value']) {
      form_set_value($element['province'], $postal_code['province'], $form_state);
    }
  }
}

/**
 * Return field keys and labels for the uwaddress component.
 */
function uw_forms_component_uwaddress_subcomponents() {
  return array(
    'addr_1' => 'Address',
    'addr_2' => 'Address line 2',
    'city' => 'City',
    'province' => 'Province/State',
    'postal_code' => 'Postal Code/ZIP(+4) code',
    'country' => 'Country',
  );
}

/**
 * Render the value of a uwaddress component.
 */
function uw_forms_component_uwaddress_display($value) {
  $output = array();
  foreach (array_keys(uw_forms_component_uwaddress_subcomponents()) as $key) {
    if (!empty($value[$key])) {
      $output[] = $value[$key];
    }
  }
  return implode(', ', $output);
}

/**
 * Implements _webform_display_component().
 */
function _webform_display_uwaddress($component, $value, $format = 'html') {
  return array(
    '#title' => $component['name'],
    '#weight' => $component['weight'],
    '#theme' => 'webform_display_textfield',
    '#theme_wrappers' => $format == 'html' ? array('webform_element') : array('webform_element_text'),
    '#field_prefix' => '',
    '#field_suffix' => '',
    '#component' => $component,
    '#format' => $format,
    '#value' => isset($value) ? uw_forms_component_uwaddress_display($value) : '',
  );
}

/**
 * Implements _webform_submit_component().
 */
function _webform_submit_uwaddress($component, $value) {
  if (!is_array($value)) {
    return NULL;
  }
  return array_map('check_plain', $value);
}

/**
 * Implements _webform_table_component().
 */
function _webform_table_uwaddress($component, $value) {
  return uw_forms_component_uwaddress_display($value);
}

/**
 * Implements _webform_csv_headers_component().
 */
function _webform_csv_headers_uwaddress($component, $export_options) {
  $header = array();
  $header[0] = array('');
  $header[1] = array(!empty($export_options['header_keys']) ? $component['form_key'] : $component['name']);
  $header[2] = array();

  $count = 0;
  foreach (uw_forms_component_uwaddress_subcomponents() as $key => $label) {
    // Empty column per sub-field in main header.
    if ($count !== 0) {
      $header[0][] = '';
      $header[1][] = '';
    }
    // The value for this option.
    $header[2][] = !empty($export_options['header_keys']) ? $key : $label;
    $count++;
  }

  return $header;
}

/**
 * Implements _webform_csv_data_component().
 */
function _webform_csv_data_uwaddress($component, $export_options, $value) {
  $return = array();
  foreach (array_keys(uw_forms_component_uwaddress_subcomponents()) as $key) {
    $return[] = isset($value[$key]) ? $value[$key] : '';
  }
  return $return;
}
